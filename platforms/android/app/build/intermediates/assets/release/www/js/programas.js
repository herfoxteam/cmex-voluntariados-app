//CONTROLADOR PROGRAMA DETALLE
app.controller('programsCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate) {
	
	//PROGRAMAS
	$scope.refreshPrograms = function(){
		$http.get('http://voluntariadocemex.com/api/programs',{timeout: 10000})
		.success(function(result){
			window.localStorage['local_programas'] = JSON.stringify(result.data);	
			$scope.Program_Process();	
		})
		.error(function(data, status, headers, config){
			$scope.Program_Process();
		});
		
		//window.location = "#/news";
		$scope.$broadcast('scroll.refreshComplete');
	}
	//PROCESAR LA DATA
	$scope.Program_Process = function(){

		$scope.programs = [];
		$scope.back_p = "";
		$scope.next_p = "";
		
		id = $stateParams.id;
			
			
			//$scope.Mostar_preloader();
		$scope.news_detail = [];
		id = $stateParams.id;
		
		result = JSON.parse(window.localStorage['local_programas']);
	
		Object.keys(result).forEach(function(key,index) {
			
			if(result[index]["id"] == id){
				$scope.programs = result[index];
				
				//console.log(result);
				id_p = parseInt(result[index]["id"]);
				
				$scope.back_p = (id_p-1);
				$scope.next_p = (id_p+1);
	
				if( $scope.back_p <= 0 ){
					$scope.back_p = 6;
				}
				
				if( $scope.next_p >= 7 ){
					$scope.next_p = 1;
				}			
			}
		});
	}

	$scope.refreshPrograms();

});