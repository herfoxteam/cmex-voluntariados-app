//CONTROLADOR TODAS LAS JORNADAS
app.controller('journeysAllCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate) {
	
	//FUNCTION PARA REFRESCAR
	$scope.refreshJourneys = function(){
		
		$scope.Mostar_preloader();
		$scope.journeys = [];
		
		$scope.id_programa = "";
		
		//CONSULTAMOS LAS JORNADAS
		$http.get('http://voluntariadocemex.com/api/journeys',{timeout: 10000})
		.success(function(result){
			
			//convertimos la imagenes a base 64
			Object.keys(result.data).forEach(function(key,index) {
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				
				request.onload = function() { };
				request.send();
			});
			
			$scope.journeys = result.data;
			window.localStorage['local_jornadas'] = JSON.stringify(result.data);
			$scope.Ocultar_preloader();
			if(window.localStorage['ios_status'] == 1){ $scope.activar = 1; }
			else{ $scope.activar = 0; }
		})
		.error(function(data, status, headers, config){
			$scope.journeys = JSON.parse(window.localStorage['local_jornadas']);
			$scope.Ocultar_preloader();
			if(window.localStorage['ios_status'] == 1){ $scope.activar = 1; }
			else{ $scope.activar = 0; }
		});
		
		result = JSON.parse(window.localStorage['local_programas']);	
		$scope.programs = result;
	
		$scope.$broadcast('scroll.refreshComplete');
	}
	
	$scope.refreshJourneys();
			
});
	
//CONTROLADOR JORNADAS POR PROGRAMA
app.controller('journeysCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate) {
	
	//FUNCTION PARA REFRESCAR
	$scope.refreshJourneys = function(){
		$scope.Mostar_preloader();
		$scope.journeys = [];
		$scope.id_programa = $stateParams.id;
		
		//CONSULTAMOS LAS JORNADAS
		$http.get('http://voluntariadocemex.com/api/journeys',{timeout: 10000})
		.success(function(result){
			
			//convertimos la imagenes a base 64
			Object.keys(result.data).forEach(function(key,index) {
				
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				
				request.onload = function() { };
				request.send();
			});
			
			$scope.journeys = result.data;
			window.localStorage['local_jornadas'] = JSON.stringify(result.data);
			$scope.Ocultar_preloader();
		})
		.error(function(data, status, headers, config){
			$scope.journeys = JSON.parse(window.localStorage['local_jornadas']);
			$scope.Ocultar_preloader();
		});
		
		result = JSON.parse(window.localStorage['local_programas']);	
		$scope.programs = result;
	
		$scope.$broadcast('scroll.refreshComplete');
	}
	
	$scope.refreshJourneys();
	
	/*
	//FUNCTION PARA REFRESCAR
	$scope.refreshJourneys = function(){
		
		$scope.Mostar_preloader();
		$scope.journeys = [];
		$scope.id_programa = $stateParams.id;
		
		//CONSULTAMOS LAS JORNADAS
		$http.get('http://voluntariadocemex.com/api/journeys',{timeout: 10000})
		.success(function(result){
			
			//convertimos la imagenes a base 64
			Object.keys(result.data).forEach(function(key,index) {
				
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				
				request.onload = function() { };
				request.send();
			});
			
			$scope.journeys = result.data;
			window.localStorage['local_jornadas'] = JSON.stringify(result.data);
			$scope.Ocultar_preloader();
		})
		.error(function(data, status, headers, config){
			$scope.journeys = JSON.parse(window.localStorage['local_jornadas']);
			$scope.Ocultar_preloader();
		});
		
		result = JSON.parse(window.localStorage['local_programas']);	
		$scope.programs = result;
		
		$scope.$broadcast('scroll.refreshComplete');
	}
	*/
			
});

//CONTROLADOR DETALLE JORNADA
app.controller('journeysDetailCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate) {
	
	//FUNCTION PARA REFRESCAR
	$scope.refreshJourneyDetail = function(){
		$scope.journey = [];
		id = $stateParams.id;
		
		result = JSON.parse(window.localStorage['local_jornadas']);
		console.log(result);
	
		Object.keys(result).forEach(function(key,index) {
			if(result[index]["id"] == id){
				$scope.journey = result[index];
			}
		});
		if(window.localStorage['ios_status'] == 1){ $scope.activar = 1; }
		else{ $scope.activar = 0; }
		
		$scope.$broadcast('scroll.refreshComplete');
	}
	
	$scope.refreshJourneyDetail();
	
	/*
	//FUNCTION PARA REFRESCAR
	$scope.refreshJourneyDetail = function(){
		
		$scope.journey = [];
		id = $stateParams.id;
		
		result = JSON.parse(window.localStorage['local_jornadas']);
		console.log(result);
	
		Object.keys(result).forEach(function(key,index) {
			if(result[index]["id"] == id){
				$scope.journey = result[index];
			}
		});
		
		$scope.$broadcast('scroll.refreshComplete');
	}
	*/

	$scope.Compartir = function(tipo, title, desc, image, start, end){ 
		link = 'https://www.cemexcolombia.com/nuestra-empresa/voluntariado';
		sms_text = 'Te invitamos a participar en nuestra nueva jornada: \n '+title+' \n '+desc+' \n Del '+start+' al '+end+' \n Organiza Voluntarios Cemex'; 
       
        if(tipo == 'face'){
            window.plugins.socialsharing.shareViaFacebook(sms_text, image, link, onSuccess, onError); 
		}
        if(tipo == 'twi'){
            window.plugins.socialsharing.shareViaTwitter(sms_text, image, link, onSuccess, onError );
		}
    }
	
	var onSuccess = function(result) {
	};
	
	var onError = function(msg) {
	  $scope.Mostar_Alert('lo sentimos, no tienes instalado el app para compartir');
	};
	
});


















/*


//CONTROLADOR JORNADAS PROGRAMA
app.controller('journeysCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate) {
	
	//CONSULTAMOS LAS JORNADAS
	$scope.journeys = [];
	$http.get('http://voluntariadocemex.com/api/journeys',{timeout: 10000})
	.success(function(result){
		
		//convertimos la imagenes a base 64
		Object.keys(result.data).forEach(function(key,index) {
			//console.log(result.data[index]["image"]);
			var request = new XMLHttpRequest();
			request.open('GET', result.data[index]["image"], true);
			request.responseType = 'blob';
			
			request.onload = function() { };
			request.send();
		});
		
		$scope.home_journeys = result.data;
		window.localStorage['local_jornadas'] = JSON.stringify(result.data);
	})
	.error(function(data, status, headers, config){
		$scope.home_journeys = JSON.parse(window.localStorage['local_jornadas']);
	});




















	
	
	
		
		$scope.Mostar_preloader();
		$scope.journeys = [];
		$http.get('http://voluntariadocemex.com/api/journeys?program_id='+$stateParams.id ,{timeout: 10000})
		.success(function(result){
			
			$scope.journeys = result.data;
			$scope.Ocultar_preloader();
			console.log($scope.journeys);
		});
		
		$scope.programs = [];
		$http.get('http://voluntariadocemex.com/api/programs_list' ,{timeout: 10000})
		.success(function(result){
			$scope.programs = result.data;
			console.log($scope.programs);
		});
		
		//FUNCTION PARA REFRESCAR
		$scope.refreshJourneys = function(){
			$scope.journeys = [];
			$http.get('http://voluntariadocemex.com/api/journeys?program_id='+$stateParams.id ,{timeout: 10000})
			.success(function(result){
				
				$scope.journeys = result.data;
				$scope.Ocultar_preloader();
				console.log($scope.journeys);
			});
			
			$scope.programs = [];
			$http.get('http://voluntariadocemex.com/api/programs_list' ,{timeout: 10000})
			.success(function(result){
				$scope.programs = result.data;
				console.log($scope.programs);
			});
			//window.location = "#/news";
			$scope.$broadcast('scroll.refreshComplete');
		}

});
	
	
	
	
	
	
	
	
	*/
	
	

