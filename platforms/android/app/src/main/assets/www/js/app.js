var app = angular.module('starter', ['ionic']); //VARIABLE SUPER GLOBAL

//AQUI CONFIGURAMOS LAS VISTAS
app.config(function($stateProvider, $urlRouterProvider){
		
	$stateProvider.state('session',{
		url:'/session',
		templateUrl:'templates/session.html'
	});
		
	$stateProvider.state('registry',{
		url:'/registry',
		templateUrl:'templates/registry.html',
		controller: 'registryCtrl'
	});

	$stateProvider.state('home',{
		url:'/home',
		templateUrl:'templates/home.html',
		controller: 'homeCtrl'
	});
		
	$stateProvider.state('profile',{
		url:'/profile',
		templateUrl:'templates/profile.html',
		controller: 'profileCtrl'
	});

	$stateProvider.state('program',{
		url:'/program/:id',
		templateUrl:'templates/program.html',
		controller: 'programsCtrl'
	});
		
	$stateProvider.state('news',{
		url:'/news/:id',
		templateUrl:'templates/news.html',
		controller: 'newsCtrl'
	});
			
	$stateProvider.state('news_detail',{
		url:'/news_detail/:id',
		templateUrl:'templates/news_detail.html',
		controller: 'newsDetailCtrl'
	});
		
	$stateProvider.state('experience_detail',{
		url:'/experience_detail/:id',
		templateUrl:'templates/experience_detail.html',
		controller: 'experienceDetailCtrl'
	});
	//jordanadas por programas
	$stateProvider.state('journeys',{
		url:'/journeys/:id',
		templateUrl:'templates/journeys.html',
		controller: 'journeysCtrl'
	});
	//todas las jornadas
	$stateProvider.state('journey_all',{
		url:'/journeys',
		templateUrl:'templates/journeys.html',
		controller: 'journeysAllCtrl'
	});
		
		
	$stateProvider.state('journeys_detail',{
		url:'/journeys_detail/:id',
		templateUrl:'templates/journeys_detail.html',
		controller: 'journeysDetailCtrl'
	});

	$stateProvider.state('donation',{
		url:'/donation',
		templateUrl:'templates/donation.html',
		controller: 'donationCtrl'
	});
		
	$stateProvider.state('upload_experience',{
		url:'/upload_experience',
		templateUrl:'templates/upload_experience.html',
		controller: 'Upload_ExperienceCtrl'
	});
		
	$stateProvider.state('proposals',{
		url:'/proposals',
		templateUrl:'templates/proposals.html',
		controller: 'proposalsCtrl'
	});
	
	$stateProvider.state('qualification',{
		url:'/qualification/:id',
		templateUrl:'templates/qualification.html',
		controller: 'qualificationCtrl'
	});
	
	$stateProvider.state('qualification_list',{
		url:'/qualification_list',
		templateUrl:'templates/qualification_list.html',
		controller: 'qualificationListCtrl'
	});
	
	$stateProvider.state('qualification_list/',{
		url:'/qualification_list',
		templateUrl:'templates/qualification_list.html',
		controller: 'qualificationListCtrl'
	});
	
	$urlRouterProvider.otherwise('/home');
	
});


//ESTE SERA EL PRIMER CONTROLADOR QUE SE EJECUTA
//ESTE SERA EL PRIMER CONTROLADOR QUE SE EJECUTA
app.controller('AppCtrl', function($scope, $http, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup ) {
	
	//PROGRAMAS
	$http.get('http://voluntariadocemex.com/api/programs',{timeout: 10000})
	.success(function(result){
		
		//convertimos la imagenes a base 64
		Object.keys(result.data).forEach(function(key,index) {
			//console.log(result.data[index]["image"]);
			var request = new XMLHttpRequest();
			request.open('GET', result.data[index]["image"], true);
			request.responseType = 'blob';
			
			request.onload = function(){ };
			request.send();
			
			//console.log(result.data[index]["image"]);
			var request = new XMLHttpRequest();
			request.open('GET', result.data[index]["icon"], true);
			request.responseType = 'blob';
			
			request.onload = function(){ };
			request.send();
		});
		window.localStorage['local_programas'] = JSON.stringify(result.data);
		$scope.programs_menu_lateral = result.data;
		console.log($scope.programs_menu_lateral);
		
	})
	.error(function(data, status, headers, config){
		$scope.programs_menu_lateral = JSON.parse(window.localStorage['local_programas']);
		console.log($scope.programs_menu_lateral);
	});

	//ACTIVAR EL MENU
	//ACTIVAR EL MENU
	$scope.toggleProjects = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};

	//INICIAR SESION
	//INICIAR SESION
	$scope.startSession = function(val) {
			
		$scope.Mostar_preloader();
		no_empleado_sesion = document.getElementById("no_empleado_sesion").value;
		if(val != ""){ no_empleado_sesion = val;}
			
		if(no_empleado_sesion != ""){ //VALIDAMOS QUE EL CAMPO NO ESTÉ VACÍO
				$http.get('http://voluntariadocemex.com/api/sesion?no_employee='+no_empleado_sesion,{timeout: 10000} )
				.success(function(result){

					$scope.Ocultar_preloader();
					
					if(result.api_status == 0){
						$scope.Mostar_Alert('El número de empleado ingresado no se encuentra registrado en la base de datos, contacta al administrador de la aplicación.');
					}
					
					else{
						if(result.status == 2){
							$scope.Mostar_Alert_Inscripcion();
							window.localStorage['name'] = result.name;
							window.localStorage['last_name'] = result.last_name ;
						}
						else{
						
							window.localStorage['id_empleado'] = result.id;
							window.localStorage['no_employee'] = result.no_employee;
							window.localStorage['no_document'] = result.document ;
							window.localStorage['name'] = result.name;
							window.localStorage['last_name'] = result.last_name ;
							window.localStorage['cellphone'] = result.cellphone ;
							
							document.getElementById("no_empleado_sesion").value = ""; //vaciamos la entrada de texto
							
							document.location.href="#/home";
							
							$scope.Validar_Sesion();
							
							//REGISTRAMOS EL TOKEN AL MOMENTO DE INICIAR SESION
							window.FirebasePlugin.grantPermission(); //SOLO IOS
							window.FirebasePlugin.getToken(function(token) {
									//aquí registramos el token
									$http.get('http://voluntariadocemex.com/api/registry_token?token='+token+'&employee_id='+result.id )
									.success(function(result){
											//$scope.home_journeys = result.data;
									});
									
									//alert(token);
									
									//alert(token);
									
								}, function(error) {
								console.error(error);
							});
						}
							//FINAL DEL REGISTRO DEL TOKEN
					}
					
				})
				.error(function(data, status, headers, config){
					$scope.Ocultar_preloader();
					$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
				});					
		}
		
		else{
			$scope.Ocultar_preloader();
			$scope.Mostar_Alert('Recuerda que debes ingresar el número de empleado');
		}
	};

	//VALIDAR LA SESION
	//VALIDAR LA SESION
	$scope.Validar_Sesion = function() {
		//VALIDAMOS LA API PARA IOS///
		$http.get('http://voluntariadocemex.com/api/ios?id=1',{timeout: 10000} )
			.success(function(result){
				
				if(result.status == 1){
					window.localStorage['ios_status'] = 1;
					setTimeout( $scope.alert_datos ,3000);
					document.location.href="#/home";
					document.getElementById("bt_quiero_donar").style.display = "none";
					document.getElementById("ico_perfil").style.display = "none";
					document.getElementById("ico_cerrar_sesion").style.display = "none";
				}
				
				if(result.status == 2){
					window.localStorage['ios_status'] = 2;
					if( window.localStorage['id_empleado'] == "" || window.localStorage['id_empleado'] == null ){
						document.location.href="#/session";
					}
					else{
						setTimeout( $scope.alert_datos ,3000);
						document.location.href="#/home";
					}
				}
		})
		.error(function(data, status, headers, config){
			
			status = window.localStorage['ios_status'];
			
			if(status == 2){
					if( window.localStorage['id_empleado'] == "" || window.localStorage['id_empleado'] == null ){
						document.location.href="#/session";
					}
					else{
						setTimeout( $scope.alert_datos ,3000);
						document.location.href="#/home";
					}
					window.localStorage['ios_status'] = 2;
			}
				
			else if(result.status == 1){
					window.localStorage['ios_status'] = 1;
					setTimeout( $scope.alert_datos ,3000);
					document.location.href="#/home";
					document.getElementById("bt_quiero_donar").style.display = "none";
					document.getElementById("ico_perfil").style.display = "none";
					document.getElementById("ico_cerrar_sesion").style.display = "none";
			}
			else{
				document.location.href="#/home";
				document.getElementById("bt_quiero_donar").style.display = "none";
				document.getElementById("ico_cerrar_sesion").style.display = "none";
				window.localStorage['ios_status'] = 1;
			}

		});	
	};
	
	//RUTA PERSONALIZADA FIREBASE
	//RUTA PERSONALIZADA FIREBASE
	$scope.alert_datos = function() {
		window.FirebasePlugin.onNotificationOpen(function(notification) {
				ruta = notification.action_destination;
				document.location.href = ruta;
				
			}, function(error) {
				alert(error);
			});
		
	};
	
	//CERRAR SESION
	//CERRAR SESION
	$scope.Cerrar_Sesion = function() {
		window.localStorage['id_empleado'] = "";
		document.location.href="#/session";
	};
		
	//IR A INSCRIPCION
	//IR A INSCRIPCION
	$scope.Go_Register = function() {
		document.location.href="#/registry";
	};
	
	//IR A SESION
	//IR A SESION
	$scope.Go_Session = function(title, message) {
		document.location.href="#/session";
	};
		
	//IR A SESION
	//IR A SESION
	$scope.Go_Jornadas = function(val) {
		//val = document.getElementById('programa_don').value;
		document.location.href="#/journeys/"+val;
	};

	//VER PROGRAMAS MENU
	//VER PROGRAMAS MENU
	$scope.VerProgramasMenu = function() {
		document.getElementById('programas_menu').style.display = 'block';
		document.getElementById('bt_mostrar_programs').style.display = 'none';
		document.getElementById('bt_ocultar_programs').style.display = 'block';
	};
		
	//OCULTAR PROGRAMAS MENU
	//OCULTAR PROGRAMAS MENU
	$scope.OcultarProgramasMenu = function() {
		document.getElementById('programas_menu').style.display = 'none';
		document.getElementById('bt_mostrar_programs').style.display = 'block';
		document.getElementById('bt_ocultar_programs').style.display = 'none';
	};
		
	//INSCRIPCION
	//INSCRIPCION
	$scope.Inscription_journey = function(id) {
			
		$scope.Mostar_preloader();
		$http.get('http://voluntariadocemex.com/api/inscriptions?employee_id='+window.localStorage['id_empleado']+'&journey_id='+id+'&status=1',{timeout: 10000})
		//$http.get('http://voluntariadocemex.com/api/inscriptions?employee_id='+window.localStorage['id_empleado']+'&journey_id='+id)
		.success(function(result){
			//console.log(result);
			$scope.Ocultar_preloader();
			$scope.Mostar_Alert('La inscripción se realizó exitosamente.' );
		})
		.error(function(data, status, headers, config){
			$scope.Ocultar_preloader();
			$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
		});
	};
		
	//VALIDAR CORREO
	$scope.ValidarEmail = function(valor) {
		if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
			return true;
		} else {
			return false;
		}
	}

	//REGISTRO
	//REGISTRO
	$scope.Registry = function() {
			
			permitir_registro = true;
			
			if( document.getElementById("nombre").value == ""){ 
				$scope.Mostar_Alert('El nombre es requerido' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("apellidos").value == ""){ 
				$scope.Mostar_Alert('Los apellidos son requerido' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("no_cedula").value == ""){ 
				$scope.Mostar_Alert('El número de documento es requerido.' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("no_celular").value == ""){ 
				$scope.Mostar_Alert('El número celular es requerido.' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("no_email").value == ""){ 
				$scope.Mostar_Alert('El correo electrónico es requerido.' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("no_ubicacion").value == ""){ 
				$scope.Mostar_Alert('La ubicación es requerida.' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("no_edad").value == ""){ 
				$scope.Mostar_Alert('La edad es requerida.' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("no_descripcion").value == ""){ 
				$scope.Mostar_Alert('La descripción es requerida.' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("talla_camiseta").value == ""){ 
				$scope.Mostar_Alert('La talla de la camiseta es requerida.' ); 
				permitir_registro = false;
			}
			
			if( document.getElementById("talla_zapatos").value == ""){ 
				$scope.Mostar_Alert('La talla de los zapatos es requerido.' ); 
				permitir_registro = false;
			}

			
			if( permitir_registro == true ){
				
				validar_mail = $scope.ValidarEmail( document.getElementById("no_email").value );
				
				if( validar_mail == false ){
					$scope.Mostar_Alert( '¡El correo electrónico es inválido.!' );
				}
				
				else{

					$scope.Mostar_preloader();
					
					var data = {
						"name": document.getElementById("nombre").value,
						"last_name": document.getElementById("apellidos").value,
						"photo": '',
						"no_employee": document.getElementById("no_empleado").value,
						"document": document.getElementById("no_cedula").value,
						"cellphone": document.getElementById("no_celular").value,
						"email": document.getElementById("no_email").value,
						"age": document.getElementById("no_edad").value,
						"description": document.getElementById("no_descripcion").value,
						"location": document.getElementById("no_ubicacion").value,
						"shirt_size": document.getElementById("talla_camiseta").value,
						"shoe_size": document.getElementById("talla_zapatos").value
					};
					
					var header = { "headers": {"Content-Type": "application/json"} };
					
						//$http.get('http://voluntariadocemex.com/api/registry?name='+name+'&last_name='+last_name+'&no_employee='+no_employee+'&document='+documento+'&cellphone='+cellphone+
						//'&email='+email+'&shirt_size='+shirt_size+'&shoe_size='+shoe_size )
					$http.post('http://voluntariadocemex.com/api/registry', data , header, {timeout: 10000} )
					.success(function(result){
							
						if(result["api_status"] == 1){
							$scope.Ocultar_preloader();
							$scope.Mostar_Alert( result["api_message"] );
							$scope.startSession( document.getElementById("no_empleado").value );
							
							//LIMPIAR LOS DATOS
							document.getElementById("nombre").value = "";
							document.getElementById("apellidos").value = "";
							document.getElementById("no_empleado").value = "";
							document.getElementById("no_cedula").value = "";
							document.getElementById("no_celular").value = "";
							document.getElementById("no_email").value = "";
							document.getElementById("no_ubicacion").value = "";
							document.getElementById("talla_camiseta").value = "";
							document.getElementById("talla_zapatos").value = "";
							
						}
						else if(result["api_status"] == 3){
							$scope.Ocultar_preloader();
							$scope.Mostar_Alert( result["api_message"] );
						}
						
						else{
							$scope.Ocultar_preloader();
							$scope.Mostar_Alert( "Lo sentimos ha ocurrido un error, por favor valida tu acceso a internet." );
						}
		
					})
					.error(function(data, status, headers, config){
						$scope.Ocultar_preloader();
						$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
					});
				}
				
			}
	};
		
		
	
	
	//DONACION
	//DONACION
	$scope.Donation = function() {
			valor_don = document.getElementById("valor_don").value;
			desde_don = document.getElementById("desde_don").value;
			hasta_don = document.getElementById("hasta_don").value;
			programa_don = document.getElementById("programa_don").value;
			nombre_dom = document.getElementById("nombre_dom").value;
			cedula_don = document.getElementById("cedula_don").value;
			cargo_don = document.getElementById("cargo_don").value;
			empresa_don = document.getElementById("empresa_don").value;
			terminacion_don = document.getElementById("terminacion_don").value;
			terminos_dom = document.getElementById("terminos_dom").checked;


			var dt = new Date();
			dia = dt.getDate();
			mes = dt.getMonth();
			horas = dt.getHours();
			minutos = +dt.getMinutes();
			segundos = dt.getSeconds();
			
			
			if(dt.getMonth() <= 9){mes = "0"+dt.getMonth(); }
			if(dt.getDate() <= 9){dia = "0"+dt.getDate(); }
			if(dt.getHours() <= 9){horas = "0"+dt.getHours(); }
			if(dt.getMinutes() <= 9){minutos = "0"+dt.getMinutes(); }
			if(dt.getSeconds() <= 9){segundos = "0"+dt.getSeconds(); }
			fecha = dt.getFullYear()+"-"+mes+"-"+dia+" "+horas+":"+minutos+":"+segundos;
			
			$scope.Mostar_preloader();

			$http.get('http://voluntariadocemex.com/api/donations?employee_id='+window.localStorage['id_empleado']+
			'&rode='+valor_don+'&start_date='+desde_don+'&end_date='+hasta_don+'&program_id='+programa_don+'&final_cause_id='+terminacion_don+'&acceptance_terms='+terminos_dom , {timeout: 10000})
			.success(function(result){
				
				if(result["api_status"] == 1){
					$scope.Ocultar_preloader();
					$scope.Mostar_Alert( result["api_message"] );
					document.location.href="#/home";
				}
				else{
					$scope.Mostar_Alert( 'Lo sentimos, ha ocurrido un error, intenta nuevamente' );
				}
				console.log(result);				
			})
			.error(function(data, status, headers, config){
				$scope.Ocultar_preloader();
				$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
			});
	};

	//VER NOTICIAS
	$scope.Ver_modulos = function(val) {
		
		var noticias = document.querySelectorAll('.list_noticias');
		noticias.forEach(function(noticia) {
			noticia.style.display = 'none';
		});
		
		var experiencias = document.querySelectorAll('.list_experiencias');
		experiencias.forEach(function(experiencia) {
			experiencia.style.display = 'none';
		});
		
		var galerias = document.querySelectorAll('.list_galerias');
		galerias.forEach(function(galeria) {
			galeria.style.display = 'none';
		});
		
		if(val == 1){
			var noticias = document.querySelectorAll('.list_noticias');
			noticias.forEach(function(noticia) {
				noticia.style.display = 'block';
			});
		}
		
		if(val == 2){
			var experiencias = document.querySelectorAll('.list_experiencias');
			experiencias.forEach(function(experiencia) {
				experiencia.style.display = 'block';
			});
		}
		
		if(val == 3){
			var galerias = document.querySelectorAll('.list_galerias');
			galerias.forEach(function(galeria) {
				galeria.style.display = 'block';
			});
		}
		
	};
	
	
	
	$scope.Ver_Noticias = function() {
		document.getElementsByClassName('list_noticias')[0].style.display = 'block';
		document.getElementsByClassName('list_experiencias')[0].style.display = 'none';
		document.getElementsByClassName('list_galerias')[0].style.display = 'none';
	};
	
	//VER EXPERIENCIAS
	$scope.Ver_Experiencias = function() {
		document.getElementsByClassName('list_noticias')[0].style.display = 'none';
		document.getElementsByClassName('list_experiencias')[0].style.display = 'block';
		document.getElementsByClassName('list_galerias')[0].style.display = 'none';
	};	
	
	//VER GALERIAS
	$scope.Ver_Galerias = function() {
		document.getElementsByClassName('list_noticias')[0].style.display = 'none';
		document.getElementsByClassName('list_experiencias')[0].style.display = 'none';
		document.getElementsByClassName('list_galerias')[0].style.display = 'block';
	};
			
	//CONVERTIR IMAGEN A BASE 64
	$scope.Procesar_Imagen = function(img) {
			
		base64 = "";
		var file = img.files[0];
		var reader = new FileReader();
		reader.onloadend = function() {
			//console.log(reader.result);
			base64 = reader.result;
			return base64;
		}
		reader.readAsDataURL(file);
	};
	
	
		
	

	//DONACION
	//DONACION
	$scope.Save_Proposal = function() {
			programa_prop = document.getElementById("programa_prop").value;
			no_empleado_prop = document.getElementById("no_empleado_prop").value;
			no_celular_prop = document.getElementById("no_celular_prop").value;
			descripcion_prop = document.getElementById("descripcion_prop").value;
			poblacion_prop = document.getElementById("poblacion_prop").value;
			fecha_prop = document.getElementById("fecha_prop").value;
			lugar_prop = document.getElementById("lugar_prop").value;
			
			$scope.Mostar_preloader();
			
			$http.get('http://voluntariadocemex.com/api/proposals?employee_id='+window.localStorage['id_empleado']+
			'&program_id='+programa_prop+'&descripction_journeys='+descripcion_prop+'&population='+poblacion_prop+'&place='+lugar_prop+'&date='+fecha_prop+'&status=2', {timeout: 10000})
			.success(function(result){
				
				if(result["api_status"] == 1){
					$scope.Ocultar_preloader();
					$scope.Mostar_Alert( result["api_message"] );
					document.location.href="#/home";
					
					//limpiamos los datos del formulario
					document.getElementById("programa_prop").value = "";
					document.getElementById("no_empleado_prop").value = "";
					document.getElementById("no_celular_prop").value = "";
					document.getElementById("descripcion_prop").value = "";
					document.getElementById("poblacion_prop").value = "";
					document.getElementById("fecha_prop").value = "";
					document.getElementById("lugar_prop").value = "";
				}
				else{
					$scope.Ocultar_preloader();
					$scope.Mostar_Alert( "lo sentimos ha ocurrido un error, intenta nuevamente." );
				}

			})
			.error(function(data, status, headers, config){
				$scope.Ocultar_preloader();
				$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
			});
	};
		

	///ALERTAS ////////////////////////////////////////////////////////////////
	///ALERTAS ////////////////////////////////////////////////////////////////
	///ALERTAS ////////////////////////////////////////////////////////////////
	///ALERTAS ////////////////////////////////////////////////////////////////
	///ALERTAS ////////////////////////////////////////////////////////////////
	//MOSTRAR ALERTA SUSCRIPCION
	//MOSTRAR ALERTA SUSCRIPCION
	$scope.Mostar_Alert_Inscripcion = function(no_emp) {

			var alertPopup = $ionicPopup.alert({
				title: '<img src="img/logo_menu.png" width="90%">',
				templateUrl: 'templates/popup_registro.html',
				cssClass: 'base_pop_up',
				buttons: [
					{ 
						text: 'Convertirme en Voluntario',
						type: 'button-assertive',
						onTap: function(e) {
						  $scope.Go_Register();
						}
					},
					{ 
						text: 'Regresar',
						type: 'button-stable',
					}
				]
			});
	};	
		
	//MOSTRAR ALERTA
	//MOSTRAR ALERTA
	$scope.Mostar_Alert = function(message) {
			var alertPopup = $ionicPopup.alert({
				title: '<img src="img/logo_menu.png" width="90%"><br>',
				template: '<div style="text-align: center; font-family: Cairo-Regular; padding-left: 7px; padding-right: 7px;">'+message+'</div>',
				cssClass: 'base_pop_up',
				buttons: [
					{ 
						text: 'Aceptar',
						type: 'button-assertive',
					}
				]
			});
	};	
		
	//MOSTRAR ALERTA DONACION
	//MOSTRAR ALERTA DONACION
	$scope.Mostar_Alert_Donacion = function() {
			
			permitir_registro = true;
			
			if(document.getElementById("valor_don").value == ""){
				$scope.Mostar_Alert( '¡El valor de la donación es requerido!' );
				permitir_registro = false;
			}
			
			if(document.getElementById("desde_don").value == ""){
				$scope.Mostar_Alert( '¡Debe ingresar una fecha de inicio de diferido.' );
				permitir_registro = false;
			}
			
			if(document.getElementById("hasta_don").value == ""){
				$scope.Mostar_Alert( '¡Debe ingresar una fecha de terminación de diferido.' );
				permitir_registro = false;
			}
			
			if(document.getElementById("programa_don").value == ""){
				$scope.Mostar_Alert( 'Debes seleccionar un programa' );
				permitir_registro = false;
			}
			
			if(document.getElementById("empresa_don").value == ""){
				$scope.Mostar_Alert( 'El campo empresa es requerido' );
				permitir_registro = false;
			}

			if(document.getElementById("terminacion_don").value == ""){
				$scope.Mostar_Alert( 'Debe seleccionar una causa de terminacion con la empresa.' );
				permitir_registro = false;
			}
			
			//alert(document.getElementById("terminos_dom").checked);
			
			if(document.getElementById("terminos_dom").checked == false){
				$scope.Mostar_Alert( 'Debes aceptar los terminos y condiciones.' );
				permitir_registro = false;
			}

			valor_don = document.getElementById("valor_don").value;
			desde_don = document.getElementById("desde_don").value;
			hasta_don = document.getElementById("hasta_don").value;
			programa_don = document.getElementById("programa_don").value;
			nombre_dom = document.getElementById("nombre_dom").value;
			cedula_don = document.getElementById("cedula_don").value;
			cargo_don = document.getElementById("cargo_don").value;
			empresa_don = document.getElementById("empresa_don").value;
			terminacion_don = document.getElementById("terminacion_don").value;
			terminos_dom = document.getElementById("terminos_dom").value;
			terminos_dom = document.getElementById("terminos_dom").checked;
			
			if( permitir_registro == true ){

			
				var alertPopup = $ionicPopup.alert({
					title: '<img src="img/logo_menu.png" width="90%">',
					templateUrl: 'templates/popup_donation.html',
					cssClass: 'base_pop_up',
					buttons: [
						{ 
							text: 'Hacer Donación',
							type: 'button-assertive',
							onTap: function(e) {
							  $scope.Donation();
							}
						},
						{ 
							text: 'Cancelar',
							type: 'button-stable',
						}
					]
				});
			}
			/*
			else{
				$scope.Mostar_Alert( '¡Recuerda que todos los datos del formulario son obligatorios!' );
			}
			*/
	};
		
	//MOSTRAR ALERTA DONACION
	//MOSTRAR ALERTA DONACION
	$scope.Mostar_Alert_Propuesta = function() {
			
			permitir_registro = true;
			
			if( document.getElementById("programa_prop").value == "" ){
				$scope.Mostar_Alert( 'Debes seleccionar un programa.' );
				permitir_registro = false;
			}
			
			if( document.getElementById("no_empleado_prop").value == "" ){
				$scope.Mostar_Alert( 'Debes seleccionar un programa.' );
				permitir_registro = false;
			}
			
			if( document.getElementById("no_celular_prop").value == "" ){
				$scope.Mostar_Alert( 'Debes ingresar un número celular de contacto.' );
				permitir_registro = false;
			}
			
			if( document.getElementById("descripcion_prop").value == "" ){
				$scope.Mostar_Alert( 'Debes ingresar una descripción.' );
				permitir_registro = false;
			}
			
			if( document.getElementById("poblacion_prop").value == "" ){
				$scope.Mostar_Alert( 'El campo población es requerido.' );
				permitir_registro = false;
			}
			
			if( document.getElementById("fecha_prop").value == "" ){
				$scope.Mostar_Alert( 'La fecha es requerida.' );
				permitir_registro = false;
			}
			
			if( document.getElementById("lugar_prop").value == "" ){
				$scope.Mostar_Alert( 'El lugar es requerido.' );
				permitir_registro = false;
			}
			
			programa_prop = document.getElementById("programa_prop").value;
			no_empleado_prop = document.getElementById("no_empleado_prop").value;
			no_celular_prop = document.getElementById("no_celular_prop").value;
			descripcion_prop = document.getElementById("descripcion_prop").value;
			poblacion_prop = document.getElementById("poblacion_prop").value;
			fecha_prop = document.getElementById("fecha_prop").value;
			lugar_prop = document.getElementById("lugar_prop").value;

			if(permitir_registro == true ){

				var alertPopup = $ionicPopup.alert({
					title: '<img src="img/logo_menu.png" width="90%">',
					templateUrl: 'templates/popup_propuesta.html',
					cssClass: 'base_pop_up',
					buttons: [
						{ 
							text: 'Subir',
							type: 'button-assertive',
							onTap: function(e) {
							  $scope.Save_Proposal();
							}
						},
						{ 
							text: 'Regresar',
							type: 'button-stable',
						}
					]
				});
			}
	};	
		
	//MOSTRAR ALERTA INSCRIPCION
	//MOSTRAR ALERTA INSCRIPCION
	$scope.Mostar_Alert_Jornada = function(val) {
			var alertPopup = $ionicPopup.alert({
				title: '<img src="img/logo_menu.png" width="90%">',
				templateUrl: 'templates/popup_inscripcion.html',
				cssClass: 'base_pop_up',
				buttons: [
					{ 
						text: 'Unirme',
						type: 'button-assertive',
						onTap: function(e) {
						  $scope.Inscription_journey(val);
						}
					},
					{ 
						text: 'Regresar',
						type: 'button-stable',
					}
				]
			});
	};
		
	//MOSTRAR PRELOADER
	$scope.Mostar_preloader = function() {
			$ionicLoading.show({
				template: 'Cargando...'
				//duration: 3000
			}).then(function(){
				//console.log("The loading indicator is now displayed");
			});
	};
		
	//OCULTAR PRELOADER
	$scope.Ocultar_preloader = function() {
			$ionicLoading.hide().then(function(){
			   console.log("The loading indicator is now hidden");
			});
	};	

		
		
	//MOSTRAR IMÁGENES
	//MOSTRAR IMÁGENES
	$scope.MostrarImagen = function(val) {
			
		$scope.swiperOptionsGalery = {
			/* Whatever options */
			effect: 'slide',
			initialSlide: val,
			/* Initialize a scope variable with the swiper */
			onInit: function(swiper){
			$scope.swiper = swiper;
			// Now you can do whatever you want with the swiper
			},
			onSlideChangeEnd: function(swiper){
				console.log('The active index is ' + swiper.activeIndex); 
			}
		};
	
		$scope.gallery_images_list = JSON.parse(window.localStorage['local_galeries']);
		//console.log($scope.gallery_images_list);
		var alertPopup = $ionicPopup.alert({
				scope: $scope,
				templateUrl: 'templates/popup_foto.html',
				cssClass: 'base_pop_up_foto',
				buttons: [
					{ 
						text: 'Regresar',
						type: 'button-stable',
					}
				]
		});
	};

	///
	$scope.Convertir_Img = function(url){
		var base_64;
		var request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.responseType = 'blob';
		
		request.onload = function() {
			var reader = new FileReader();
			reader.readAsDataURL(request.response);
			reader.onload =  function(e){
				base_64 =  e.target.result;
				return base_64;
				//console.log('DataURL:', e.target.result);
			};
			return base_64;
		};
		
		request.send();
		return base_64;
		//console.log( base_64);
	};
	
	
	
	//MOSTRAR MIS EXPERIENCIAS
	//MOSTRAR MIS EXPERIENCIAS
	//MOSTRAR MIS EXPERIENCIAS
	$scope.My_Experiences = function(){

		var experiencias = document.querySelectorAll('.others');
		experiencias.forEach(function(experiencia) {
			experiencia.parentElement.style.display = 'none';
			//experiencia.style.display = 'none';
		});
	}
	
	$scope.All_Experiences = function(){

		var experienciasAll = document.querySelectorAll('.others');
		experienciasAll.forEach(function(experiencia) {
			experiencia.parentElement.style.display = 'block';
			//experiencia.style.display = 'none';
		});
	}

	//EJECUTAMOS LA VALIDACION DE LA SESION
	$scope.Validar_Sesion();
	//$scope.getToken();	
	
});

//FUNCTION PARA CORRER LOS PLUGING
app.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		if(window.cordova && window.cordova.plugins.Keyboard) {
		  // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		  // for form inputs)
		  cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		  // Don't remove this line unless you know what you are doing. It stops the viewport
		  // from snapping when text inputs are focused. Ionic handles this internally for
		  // a much nicer keyboard experience.
		  cordova.plugins.Keyboard.disableScroll(true);
		}
		
		if(window.StatusBar) {
		  StatusBar.styleDefault();
		}
		//db = $cordovaSQLite.openDB("database.db");
        //$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS product (id integer primary key, firstname text, lastname text)");
	});		
});

app.factory('Camera', function($q) {
   return {
      getPicture: function(options) {
         var q = $q.defer();

         navigator.camera.getPicture(function(result) {
            q.resolve(result);
         }, function(err) {
            q.reject(err);
         }, options);

         return q.promise;
      }
   }
});


