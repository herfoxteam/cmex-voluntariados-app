//CONTROLADOR HOME
app.controller('homeCtrl', function($scope, $http, $ionicSideMenuDelegate, $timeout) {
	
	$scope.goToFirstSlide = function(){
		$scope.swiper.slideTo(0);
	};
		  
	$scope.swiperOptions = {
		/* Whatever options */
		effect: 'slide',
		initialSlide: 0,
		/* Initialize a scope variable with the swiper */
		onInit: function(swiper){
			$scope.swiper = swiper;
		},
		onSlideChangeEnd: function(swiper){
			//console.log('The active index is ' + swiper.activeIndex); 
		}
	};
	
	//FUNCTION PARA REFRESCAR
	$scope.refreshHome = function(){
		
		//CONSULTAMOS LOS BANNERS
		//CONSULTAMOS LOS BANNERS
		$scope.home_banner = [];
		$http.get('http://voluntariadocemex.com/api/home_banner',{timeout: 10000})
		.success(function(result){
	
			//cargamos las imágenes en temporal
			Object.keys(result.data).forEach(function(key,index) {
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				request.onload = function() { };
				request.send();
			});
			$scope.home_banner = result.data;
			window.localStorage['local_banner'] = JSON.stringify(result.data);
			
		})
		.error(function(data, status, headers, config){
			$scope.home_banner = JSON.parse(window.localStorage['local_banner']);
		});
		
		//CONSULTAMOS LAS JORNADAS
		//CONSULTAMOS LAS JORNADAS
		$scope.home_journeys = [];
		$http.get('http://voluntariadocemex.com/api/journeys',{timeout: 10000})
		.success(function(result){
			
			//convertimos la imagenes a base 64
			Object.keys(result.data).forEach(function(key,index) {
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				
				request.onload = function() { };
				request.send();
			});
			
			$scope.home_journeys = result.data;
			window.localStorage['local_jornadas'] = JSON.stringify(result.data);
			
			////////PARA VALIDAR IOS
			////////PARA VALIDAR IOS
			if(window.localStorage['ios_status'] == 1){ $scope.activar = 1; }
			else{ $scope.activar = 0; }
		})
		.error(function(data, status, headers, config){
			$scope.home_journeys = JSON.parse(window.localStorage['local_jornadas']);
			if(window.localStorage['ios_status'] == 1){ $scope.activar = 1; }
			else{ $scope.activar = 0; }
		});
		
		$scope.$broadcast('scroll.refreshComplete');
	}
	
	$scope.refreshHome(); //EJECUTAMOS EL REFRESHER LA PRIMERA VEX
	
	
	
	$scope.Contenido_Actualizado_General = function(){

		//NOTICIAS
		$http.get('http://voluntariadocemex.com/api/news ',{timeout: 10000})
		.success(function(result){
			
			//convertimos la imagenes a base 64
			Object.keys(result.data).forEach(function(key,index) {
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				
				request.onload = function(){ };
				request.send();
				
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["icon"], true);
				request.responseType = 'blob';
				
				request.onload = function(){ };
				request.send();
			});
			window.localStorage['local_news'] = JSON.stringify(result.data);
			
		});
		
		//GALERIAS
		$http.get('http://voluntariadocemex.com/api/galleries',{timeout: 10000})
		.success(function(result){
			
			//convertimos la imagenes a base 64
			Object.keys(result.data).forEach(function(key,index) {
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				
				request.onload = function(){ };
				request.send();
				
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["icon"], true);
				request.responseType = 'blob';
				
				request.onload = function(){ };
				request.send();
			});
			window.localStorage['local_galeries'] = JSON.stringify(result.data);
			
		});
		
		//EXPERICIENCIAS
		$http.get('http://voluntariadocemex.com/api/experiences?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000})
		.success(function(result){
			
			//convertimos la imagenes a base 64
			Object.keys(result.data).forEach(function(key,index) {
				//console.log(result.data[index]["image"]);
				var request = new XMLHttpRequest();
				request.open('GET', result.data[index]["image"], true);
				request.responseType = 'blob';
				
				request.onload = function(){ };
				request.send();
			});
			window.localStorage['local_experiences'] = JSON.stringify(result.data);
			
		});
	
	}
	
	$scope.Contenido_Actualizado_General();

});