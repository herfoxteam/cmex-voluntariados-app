//CONTROLADOR PARA LAS NOTICIAS
app.controller('newsCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate) {
	
	$scope.refreshNews = function(){

		$scope.Mostar_preloader();
			
		$scope.news_list = [];
		$scope.experiences_list = [];
		$scope.gallery_images_list = [];
			
		//NOTICIAS LISTA
		$http.get('http://voluntariadocemex.com/api/news' ,{timeout: 10000} )
		.success(function(result){
			$scope.news_list = result.data;
			window.localStorage['local_news'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.news_list = JSON.parse(window.localStorage['local_news']);
		});
			
		//EXPERIECIAS LISTA
		$http.get('http://voluntariadocemex.com/api/experiences?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
		.success(function(result){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = result.data;
			window.localStorage['local_experiences'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = JSON.parse(window.localStorage['local_experiences']);
		});
		
		//GALERIAS LISTAS	
		$http.get('http://voluntariadocemex.com/api/gallery_images' ,{timeout: 10000} )
		.success(function(result){
			$scope.gallery_images_list = result.data;
			$scope.Ocultar_preloader();
			window.localStorage['local_galeries'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.gallery_images_list = JSON.parse(window.localStorage['local_galeries']);
			$scope.Ocultar_preloader();
		});
		
		if(window.localStorage['ios_status'] == 1){ $scope.activar = 1; }
		else{ $scope.activar = 0; }
		
		//window.location = "#/news";
		$scope.$broadcast('scroll.refreshComplete');
	}
	
	$scope.refreshNews();
	/*	
	//FUNCTION PARA REFRESCAR
	//FUNCTION PARA REFRESCAR
	//FUNCTION PARA REFRESCAR
	$scope.refreshNews = function(){
		
		//NOTICIAS
		$http.get('http://voluntariadocemex.com/api/news' ,{timeout: 10000} )
		.success(function(result){
			$scope.news_list = result.data;
			window.localStorage['local_news'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.news_list = JSON.parse(window.localStorage['local_news']);
		});
			
		//EXPERIECIAS
		$http.get('http://voluntariadocemex.com/api/experiences?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
		.success(function(result){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = result.data;
			window.localStorage['local_experiences'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = JSON.parse(window.localStorage['local_experiences']);
			
		});
		
		//GALERIAS	
		$http.get('http://voluntariadocemex.com/api/gallery_images' ,{timeout: 10000} )
		.success(function(result){
			$scope.gallery_images_list = result.data;
			$scope.Ocultar_preloader();
			window.localStorage['local_galeries'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.gallery_images_list = JSON.parse(window.localStorage['local_galeries']);
			$scope.Ocultar_preloader();
		});
			
		//window.location = "#/news";
		$scope.$broadcast('scroll.refreshComplete');
	}
	*/
	
	//VALIDAMOS LA SECCION
	if($stateParams.id == 1){
		$scope.Ver_modulos(1);
	}

	if($stateParams.id == 2){
		$scope.Ver_modulos(2);
		$scope.My_Experiences();
	}

});



	
	
//CONTROLADOR DETALLE DE UNA NOTICIA
app.controller('newsDetailCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate) {

	//$scope.Mostar_preloader();
	$scope.news_detail = [];
	id = $stateParams.id;
	
	result = JSON.parse(window.localStorage['local_news']);
	console.log(result);
		
	Object.keys(result).forEach(function(key,index) {
		
		if(result[index]["id"] == id){
			$scope.news_detail = result[index];
		}
	});
	
	//FUNCTION PARA REFRESCAR
	$scope.refreshNewsDetail = function(){
		
		
		//NOTICIAS LISTA
		$http.get('http://voluntariadocemex.com/api/news' ,{timeout: 10000} )
		.success(function(result){
			$scope.news_list = result.data;
			window.localStorage['local_news'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.news_list = JSON.parse(window.localStorage['local_news']);
		});
		
		
		//$scope.Mostar_preloader();
		$scope.news_detail = [];
		id = $stateParams.id;
		
		result = JSON.parse(window.localStorage['local_news']);
		console.log(result);

		Object.keys(result).forEach(function(key,index) {
			
			if(result[index]["id"] == id){
				$scope.news_detail = result[index];
			}
	
		});
		$scope.$broadcast('scroll.refreshComplete');
	}
		
});

//CONTROLADOR DETALLE EXPERIENCIA
app.controller('experienceDetailCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate, $ionicPopup) {
	
	if(window.localStorage['ios_status'] == 1){ $scope.activar = 1; }
	else{ $scope.activar = 0; }

	//EXPERIECIAS LISTA
	$http.get('http://voluntariadocemex.com/api/experiences?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
		.success(function(result){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = result.data;
			window.localStorage['local_experiences'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = JSON.parse(window.localStorage['local_experiences']);
	});
	
	
	//$scope.Mostar_preloader();
	$scope.experience_detail = [];
	id = $stateParams.id;
	
	result = JSON.parse(window.localStorage['local_experiences']);
	console.log(result);

	Object.keys(result).forEach(function(key,index) {
		if(result[index]["id"] == id){
			$scope.experience_detail = result[index];
		}
	});
	
	
	//MOSTRAR IMÁGENES
	//MOSTRAR IMÁGENES
	$scope.MostrarImagen_Experiencia = function(val) {
			
		$scope.swiperOptionsExperience = {
			/* Whatever options */
			effect: 'slide',
			initialSlide: val,
			/* Initialize a scope variable with the swiper */
			onInit: function(swiper){
			$scope.swiper = swiper;
			// Now you can do whatever you want with the swiper
			},
			onSlideChangeEnd: function(swiper){
				console.log('The active index is ' + swiper.activeIndex); 
			}
		};
	
		//console.log($scope.gallery_images_list);
		var alertPopup = $ionicPopup.alert({
				scope: $scope,
				templateUrl: 'templates/popup_foto_experiencia.html',
				cssClass: 'base_pop_up_foto',
				buttons: [
					{ 
						text: 'Regresar',
						type: 'button-stable',
					}
				]
		});
	};
	

	
	//FUNCTION PARA REFRESCAR
	$scope.insertLike = function(val){
		$http.get('http://voluntariadocemex.com/api/like_instert?experience_id='+val+'&employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
		.success(function(result){
			console.log(result.api_message);
			
			if(result.api_message == "success"){
				$scope.Mostar_Alert('Hemos registrado tu like..');
				
				
				//RECARGAR EXPERIECIAS
				$http.get('http://voluntariadocemex.com/api/experiences?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
				.success(function(result){
					window.localStorage['local_experiences'] = JSON.stringify(result.data);
					$scope.refreshExperieceDetail();
					//document.location.href="#/experience_detail/"+val;
				})
				.error(function(data, status, headers, config){
					$scope.id_empleado = window.localStorage['id_empleado'];
					$scope.experiences_list = JSON.parse(window.localStorage['local_experiences']);
				});	
				
				
			}
			else{
				$scope.Mostar_Alert('Tu like ya se encuentra registrado.');
			}			
			//$scope.gallery_images_list = result.data;
			//$scope.Ocultar_preloader();
			//window.localStorage['local_galeries'] = JSON.stringify(result.data);
		})
	}
	
	//compartir
	$scope.Compartir_Experiencia = function(tipo, title, desc, image, date){ 
		link = 'https://www.cemexcolombia.com/nuestra-empresa/voluntariado';
		sms_text = title+' \n '+desc+' \n Fecha: '+date; 
		//alert(sms_text);
        if(tipo == 'face'){
            window.plugins.socialsharing.shareViaFacebook(sms_text, image, link, onSuccess, onError); 
		}
        if(tipo == 'twi'){
            window.plugins.socialsharing.shareViaTwitter(sms_text, image, link, onSuccess, onError );
		}
    }
	
	
	var onSuccess = function(result) {
	  //alert("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
	  //alert("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
	};
	
	var onError = function(msg) {
	  //alert("Sharing failed with message: " + msg);
	  $scope.Mostar_Alert('lo sentimos, no tienes instalado el app para compartir');
	};
	
	
	
	
	
	//FUNCTION PARA REFRESCAR
	$scope.refreshExperieceDetail = function(){
		
		//EXPERIECIAS LISTA
		$http.get('http://voluntariadocemex.com/api/experiences?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
		.success(function(result){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = result.data;
			window.localStorage['local_experiences'] = JSON.stringify(result.data);
		})
		.error(function(data, status, headers, config){
			$scope.id_empleado = window.localStorage['id_empleado'];
			$scope.experiences_list = JSON.parse(window.localStorage['local_experiences']);
		});
	
		//$scope.Mostar_preloader();
		$scope.experience_detail = [];
		id = $stateParams.id;
		
		result = JSON.parse(window.localStorage['local_experiences']);
		console.log(result);
	
		Object.keys(result).forEach(function(key,index) {
			if(result[index]["id"] == id){
				$scope.experience_detail = result[index];
			}
		});
	
	
		$scope.$broadcast('scroll.refreshComplete');
	}
	
	
});

