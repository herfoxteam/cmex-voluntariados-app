// JavaScript Document
//CONTROLADOR PROGRAMA DETALLE
app.controller('qualificationCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate, $ionicPopup) {
	
	id = $stateParams.id;
	$scope.jorney = [];
	$scope.question = [];
	
	//NOMBRE JORNADA
	$http.get('http://voluntariadocemex.com/api/journeys_detail?id='+id ,{timeout: 10000} )
	.success(function(result){
		//console.log(result);
		$scope.jorney = result;
	})
	.error(function(data, status, headers, config){
	});
	
	//NOMBRE JORNADA
	$http.get('http://voluntariadocemex.com/api/questions',{timeout: 10000} )
	.success(function(result){
		$scope.question = result;
		console.log(result);
	})
	.error(function(data, status, headers, config){
	});
	
	//VALIDAR EL FORMULARIO DE CALIFICACIÓN
	$scope.Validar_Formulario = function() {
		
		//alert(document.getElementById("pregunta_1").value);
		if(document.getElementById("pregunta_1").value != ""){
			
			//SI PARTICIPÓ
			if(document.getElementById("pregunta_1").value == "Si"){
				
				permitir_registro = true;
			
				if( document.getElementById("pregunta_2").value == "" ){
					permitir_registro = false;
					$scope.Mostar_Alert( "Debe calificar la experiencia." );
				}
				if( document.getElementById("pregunta_3_1").value == "" ){
					permitir_registro = false;
					$scope.Mostar_Alert( "Debe ingresar la primera palabra." );
				}
				if( document.getElementById("pregunta_3_2").value == "" ){
					permitir_registro = false;
					$scope.Mostar_Alert( "Debe ingresar la segunda palabra." );
				}
				if( document.getElementById("pregunta_3_3").value == "" ){
					permitir_registro = false;
					$scope.Mostar_Alert( "Debe ingresar la tercera palabra." );
				}
				
				if( document.getElementById("pregunta_4").value == "" ){
					permitir_registro = false;
					$scope.Mostar_Alert( "Debe ingresar el impacto que deja esta experiencia." );
				}
				if( document.getElementById("pregunta_5").value == "" ){
					permitir_registro = false;
					$scope.Mostar_Alert( "Debe seleccionar si ha cambiado o no su percepción." );
				}
				if( document.getElementById("pregunta_6").value == "" ){
					permitir_registro = false;
					$scope.Mostar_Alert( "Debe ingresar un comentario." );
				}
				
				//SOLO EN CASO DE TENER TODOS LOS CAMPOS DILIGENCIADOS
				if(permitir_registro == true){
					$scope.Mostar_Alert_Calificacion();
				}
			}
			
			//DE LO CONTRARIO
			if(document.getElementById("pregunta_1").value == "No"){
				$scope.Mostar_Alert_Calificacion();
			}
			
		}
		
		else{
			$scope.Mostar_Alert('Debe seleccionar si asistío o no a la jornada.');
		}
	
	}
	
	
	//MOSTRAR ALERTA INSCRIPCION
	//MOSTRAR ALERTA INSCRIPCION
	$scope.Mostar_Alert_Calificacion = function() {
		var alertPopup = $ionicPopup.alert({
				title: '<img src="img/logo_menu.png" width="90%">',
				templateUrl: 'templates/popup_calificar.html',
				cssClass: 'base_pop_up',
				buttons: [
					{ 
						text: 'Calificar',
						type: 'button-assertive',
						onTap: function(e) {
						  $scope.Registrar_Calificacion();
						}
					},
					{ 
						text: 'Regresar',
						type: 'button-stable',
					}
				]
		});
	};

	//MOSTRAR ALERTA INSCRIPCION
	//MOSTRAR ALERTA INSCRIPCION
	$scope.Registrar_Calificacion = function() {
		
		$scope.Mostar_preloader();

		var data = {
			"journeys_id": id,
			"employees_id": window.localStorage['id_empleado'],
			"question_1": document.getElementById("pregunta_1").value,
			"question_2": document.getElementById("pregunta_2").value,
			"question_3_1": document.getElementById("pregunta_3_1").value,
			"question_3_2": document.getElementById("pregunta_3_2").value,
			"question_3_3": document.getElementById("pregunta_3_3").value,
			"question_4": document.getElementById("pregunta_4").value,
			"question_5": document.getElementById("pregunta_5").value,
			"question_6": document.getElementById("pregunta_6").value
		};
					
		var header = { "headers": {"Content-Type": "application/json"} };

		$http.post('http://voluntariadocemex.com/api/registry_qualification', data , header, {timeout: 10000} )
			.success(function(result){
				$scope.Ocultar_preloader();
				//console.log(result);
				$scope.Mostar_Alert('Hemos registrado tu calificación.');
				window.location.href = "#/qualification_list/1";


			})
			.error(function(data, status, headers, config){
				$scope.Ocultar_preloader();
				$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
			});
	}
	
	//CALIFICAR ESTRELLAS
	//CALIFICAR ESTRELLAS
	$scope.Calificar_Estrellas = function(val) {
		document.getElementById("star_1").src = "img/star_gray.png"; 
		document.getElementById("star_2").src = "img/star_gray.png"; 
		document.getElementById("star_3").src = "img/star_gray.png"; 
		document.getElementById("star_4").src = "img/star_gray.png"; 
		document.getElementById("star_5").src = "img/star_gray.png"; 
		
		if(val == 1){
			document.getElementById("star_1").src = "img/star_yellow.png"; 
			document.getElementById("star_2").src = "img/star_gray.png"; 
			document.getElementById("star_3").src = "img/star_gray.png"; 
			document.getElementById("star_4").src = "img/star_gray.png"; 
			document.getElementById("star_5").src = "img/star_gray.png"; 
		}
		
		if(val == 2){
			document.getElementById("star_1").src = "img/star_yellow.png"; 
			document.getElementById("star_2").src = "img/star_yellow.png"; 
			document.getElementById("star_3").src = "img/star_gray.png"; 
			document.getElementById("star_4").src = "img/star_gray.png"; 
			document.getElementById("star_5").src = "img/star_gray.png"; 
		}
		if(val == 3){
			document.getElementById("star_1").src = "img/star_yellow.png"; 
			document.getElementById("star_2").src = "img/star_yellow.png"; 
			document.getElementById("star_3").src = "img/star_yellow.png"; 
			document.getElementById("star_4").src = "img/star_gray.png"; 
			document.getElementById("star_5").src = "img/star_gray.png"; 
		}
		if(val == 4){
			document.getElementById("star_1").src = "img/star_yellow.png"; 
			document.getElementById("star_2").src = "img/star_yellow.png"; 
			document.getElementById("star_3").src = "img/star_yellow.png"; 
			document.getElementById("star_4").src = "img/star_yellow.png"; 
			document.getElementById("star_5").src = "img/star_gray.png"; 
		}
		if(val == 5){
			document.getElementById("star_1").src = "img/star_yellow.png"; 
			document.getElementById("star_2").src = "img/star_yellow.png"; 
			document.getElementById("star_3").src = "img/star_yellow.png"; 
			document.getElementById("star_4").src = "img/star_yellow.png"; 
			document.getElementById("star_5").src = "img/star_yellow.png"; 
		}
		
		document.getElementById("pregunta_2").value = val;
	}
	
	
	
	$scope.journeys_list = [];
	
	//CALIFICAR ESTRELLAS
	//CALIFICAR ESTRELLAS
	$scope.refreshQualificationList = function(val) {
	
		//NOTICIAS LISTA
		$http.get('http://voluntariadocemex.com/api/journeys_qualification?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
		.success(function(result){
			console.log(result.data);
			$scope.journeys_list = result.data;
		})
		.error(function(data, status, headers, config){
			
		});
		$scope.$broadcast('scroll.refreshComplete');
		
	}
	
	$scope.refreshQualificationList();
	

});


// JavaScript Document
//CONTROLADOR PROGRAMA DETALLE
app.controller('qualificationListCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate, $ionicPopup) {
	
	$scope.journeys_list = [];
	
	//CALIFICAR ESTRELLAS
	//CALIFICAR ESTRELLAS
	$scope.refreshQualificationList = function(val) {
	
		//NOTICIAS LISTA
		$http.get('http://voluntariadocemex.com/api/journeys_qualification?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
		.success(function(result){
			console.log(result.data);
			$scope.journeys_list = result.data;
		})
		.error(function(data, status, headers, config){
			
		});
		$scope.$broadcast('scroll.refreshComplete');
		
	}
	
	$scope.refreshQualificationList();

});



