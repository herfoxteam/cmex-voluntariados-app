//CONTROLADOR DETALLE EXPERIENCIA
app.controller('Upload_ExperienceCtrl', function($scope, $http, $stateParams, $ionicSideMenuDelegate, Camera) {
	
	window.localStorage['imagen_1'] = '';
	window.localStorage['imagen_2'] = '';
	window.localStorage['imagen_3'] = '';
	window.localStorage['imagen_4'] = '';
	window.localStorage['imagen_5'] = '';

	result = JSON.parse(window.localStorage['local_programas']);	
	$scope.programs = result;
	
	var posicion = 0;
	//PARA CARGAR LAS 5 FOTOS TEMPORALMENTE
	$scope.Imagen_Temporal = function(){
		
		posicion = 6;

		if(window.localStorage['imagen_5'] == ''){ posicion = 5; }
		if(window.localStorage['imagen_4'] == ''){ posicion = 4; }
		if(window.localStorage['imagen_3'] == ''){ posicion = 3; }
		if(window.localStorage['imagen_2'] == ''){ posicion = 2; }
		if(window.localStorage['imagen_1'] == ''){ posicion = 1; }
		
		//contador_imagenes++;

		if(posicion <= 5){
		
			base64 = "";
			var file = document.getElementById("imagen_ex").files[0];
			var reader = new FileReader();
			reader.onloadend = function() {
				//console.log(reader.result);
				base64 = reader.result;
									
				//document.getElementById("imagen_perfil").files[0];
				//document.getElementById("foto_new").setAttribute('src',base64);
					
				part = base64.split(",");
				base64 = part[1];	
				window.localStorage['imagen_'+posicion] = base64;
				alert(window.localStorage['imagen_'+posicion]);
					
				document.getElementById("img_"+posicion).style.display = "block";
				document.getElementById("name_img_"+posicion).innerHTML  = file.name;
				//contador_imagenes++;
					
			}
			reader.readAsDataURL(file);		
		}
		else{
			contador_imagenes = 5;
			$scope.Mostar_Alert('lo sentimos, solo puedes cargar un máximo de 5 imágenes');
		}
	};
	
	//PARA ELIMINAR LAS FOTOS CARGADAS
	$scope.Eliminar_Foto = function(val){
		window.localStorage['imagen_'+val] = "";
		//contador_imagenes = val;
		document.getElementById("img_"+val).style.display = "none";
	};
	
	//INSCRIPCION
	//INSCRIPCION
	$scope.Subir_Experiencia = function(id) {
			
		permitir_registro = true;
			
		if( document.getElementById("programa_insc").value == "" ){
			permitir_registro = false;
			$scope.Mostar_Alert( "Debe seleccionar un programa." );
		}
			
		if( document.getElementById("titulo_insc").value == "" ){
			permitir_registro = false;
			$scope.Mostar_Alert( "El titulo es requerido." );
		}
			
		if( document.getElementById("lugar_insc").value == "" ){
			permitir_registro = false;
			$scope.Mostar_Alert( "El lugar es requerido." );
		}
			
		if( document.getElementById("fecha_insc").value == "" ){
			permitir_registro = false;
			$scope.Mostar_Alert( "La fecha es requerida." );
		}
			
		if( document.getElementById("descripcion_insc").value == "" ){
			permitir_registro = false;
			$scope.Mostar_Alert( "La descripción es requerida." );
		}
			
		if( window.localStorage['imagen_1'] == "" && window.localStorage['imagen_2'] == "" && window.localStorage['imagen_3'] == "" && window.localStorage['imagen_4'] == "" && window.localStorage['imagen_5'] == "" ){
			permitir_registro = false;
			$scope.Mostar_Alert( "Al menos una imágen es requerida." );
		}
			
		if( permitir_registro == true ){
			
			$scope.Mostar_preloader();
			
		
			var data = {
				"employee_id": window.localStorage['id_empleado'],
				"program_id": document.getElementById("programa_insc").value,
				"title": document.getElementById("titulo_insc").value,
				"place": document.getElementById("lugar_insc").value,
				"date": document.getElementById("fecha_insc").value,
				"description": document.getElementById("descripcion_insc").value,
				"image": window.localStorage['imagen_1'],
				"image_2": window.localStorage['imagen_2'],
				"image_3": window.localStorage['imagen_3'],
				"image_4": window.localStorage['imagen_4'],
				"image_5": window.localStorage['imagen_5'],
				"status":"2",
				"active":"2"
			};
					
			var header = { "headers": {"Content-Type": "application/json"} };
				
			$http.post('http://voluntariadocemex.com/api/insert_experience', data , header, {timeout: 10000} )
			.success(function(result){
						
				if(result["api_status"] == 1){
					
					id_exp = result["id"];

					//RECARGAMOS DE NUEVO LAS EXPERIECIAS Y LUEGO VAMOS A LA FICHA
					$http.get('http://voluntariadocemex.com/api/experiences?employee_id='+window.localStorage['id_empleado'] ,{timeout: 10000} )
					.success(function(result){
						window.localStorage['local_experiences'] = JSON.stringify(result.data);
						document.location.href="#/experience_detail/"+id_exp;
						
					});
					  
					$scope.Ocultar_preloader();
					$scope.Mostar_Alert( result["api_message"] );
				}
				else{
					$scope.Ocultar_preloader();
					$scope.Mostar_Alert( "lo sentimos ha ocurrido un error, intenta nuevamente." );
				}
	
			})
			.error(function(data, status, headers, config){
				$scope.Ocultar_preloader();
				$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
			});
			
		}
	}
	
	//tomar foto
	$scope.takePicture = function (options) {
	  
		var options = {
			quality : 40,
			targetWidth: 600,
			targetHeight: 600,
			sourceType: 1,
			destinationType:0
		};

		Camera.getPicture(options).then(function(imageData) {
			
				base64 = imageData;
				//inicia
				//inicia
				posicion = 6;
				if(window.localStorage['imagen_5'] == ''){ posicion = 5; }
				if(window.localStorage['imagen_4'] == ''){ posicion = 4; }
				if(window.localStorage['imagen_3'] == ''){ posicion = 3; }
				if(window.localStorage['imagen_2'] == ''){ posicion = 2; }
				if(window.localStorage['imagen_1'] == ''){ posicion = 1 }
				
				//contador_imagenes++;
				if(posicion <= 5){
					window.localStorage['imagen_'+posicion] = base64;
					document.getElementById("img_"+posicion).style.display = "block";
					document.getElementById("name_img_"+posicion).innerHTML  = 'Imágen '+posicion;
				}
				else{
					contador_imagenes = 5;
					$scope.Mostar_Alert('lo sentimos, solo puedes cargar un máximo de 5 imágenes');
				}
				//termina
				//termina
	 
			}, function(err) {
				//alert(err)
			 console.log(err);
		});
	};
   
	//tomar de la galeria
	$scope.takeGalery = function (options) {
		var options = {
			quality : 40,
			targetWidth: 600,
			targetHeight: 600,
			sourceType: 0,
			mediaType:0,
			destinationType:0
		};

		Camera.getPicture(options).then(function(imageData) {
				
				base64 = imageData;
				//inicia
				//inicia
				posicion = 6;
				if(window.localStorage['imagen_5'] == ''){ posicion = 5; }
				if(window.localStorage['imagen_4'] == ''){ posicion = 4; }
				if(window.localStorage['imagen_3'] == ''){ posicion = 3; }
				if(window.localStorage['imagen_2'] == ''){ posicion = 2; }
				if(window.localStorage['imagen_1'] == ''){ posicion = 1 }
				
				//contador_imagenes++;
				if(posicion <= 5){
					window.localStorage['imagen_'+posicion] = base64;
					document.getElementById("img_"+posicion).style.display = "block";
					document.getElementById("name_img_"+posicion).innerHTML  = 'Imágen '+posicion;
				}
				else{
					contador_imagenes = 5;
					$scope.Mostar_Alert('lo sentimos, solo puedes cargar un máximo de 5 imágenes');
				}
				//termina
				//termina
			
			}, function(err) {
			console.log(err);
		});
	}; 
	   
  
	
	
});