//CONTROLADOR PERFIL
//CONTROLADOR PERFIL
app.controller('profileCtrl', function($scope, $http, $ionicSideMenuDelegate, $ionicLoading, Camera) {

	$scope.Mostar_preloader();
	
	//FUNCTION PARA REFRESCAR
	//FUNCTION PARA REFRESCAR
	$scope.refreshProfile = function(){
			
		no_employee = window.localStorage['no_employee'];
		$scope.profile = [];
		$http.get('http://voluntariadocemex.com/api/profile?no_employee='+no_employee,{timeout: 10000})
		.success(function(result){
			$scope.profile = result;
			$scope.Ocultar_preloader();
			window.localStorage['perfil_local'] = JSON.stringify(result);
		})
		.error(function(data, status, headers, config){
			$scope.profile = JSON.parse(window.localStorage['perfil_local']);
			$scope.Ocultar_preloader();
		});
		$scope.$broadcast('scroll.refreshComplete');
	}
	
	$scope.refreshProfile();

	
	//REGISTRO
	//REGISTRO
	$scope.Edit_Profile = function() {
			
		permitir_registro = true;
			
		if( document.getElementById("edit_nombre").value == ""){ 
			$scope.Mostar_Alert('El nombre es requerido' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_apellidos").value == ""){ 
			$scope.Mostar_Alert('Los apellidos son requerido' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_documento").value == ""){ 
			$scope.Mostar_Alert('El número de documento es requerido.' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_celular").value == ""){ 
			$scope.Mostar_Alert('El número celular es requerido.' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_email").value == ""){ 
			$scope.Mostar_Alert('El correo electrónico es requerido.' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_ubicacion").value == ""){ 
			$scope.Mostar_Alert('La ubicación es requerida.' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_edad").value == ""){ 
			$scope.Mostar_Alert('La edad es requerida.' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_descripcion").value == ""){ 
			$scope.Mostar_Alert('La descripción es requerida.' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_talla_camiseta").value == ""){ 
			$scope.Mostar_Alert('La talla de la camiseta es requerida.' ); 
			permitir_registro = false;
		}
			
		if( document.getElementById("edit_talla_zapatos").value == ""){ 
			$scope.Mostar_Alert('La talla de los zapatos es requerido.' ); 
			permitir_registro = false;
		}

		if( permitir_registro == true ){
				
			validar_mail = $scope.ValidarEmail( document.getElementById("edit_email").value );
				
			if( validar_mail == false ){
				$scope.Mostar_Alert( '¡El correo electrónico es inválido.!' );
			}
				
			else{

					$scope.Mostar_preloader();
					
					var data = {
						"id": window.localStorage['id_empleado'],
						"name": document.getElementById("edit_nombre").value,
						"last_name": document.getElementById("edit_apellidos").value,
						"photo": window.localStorage['foto_perfil_tmp'],
						"no_employee": window.localStorage['no_employee'],
						"document": document.getElementById("edit_documento").value,
						"cellphone": document.getElementById("edit_celular").value,
						"email": document.getElementById("edit_email").value,
						"age": document.getElementById("edit_edad").value,
						"description": document.getElementById("edit_descripcion").value,
						"location": document.getElementById("edit_ubicacion").value,
						"shirt_size": document.getElementById("edit_talla_camiseta").value,
						"shoe_size": document.getElementById("edit_talla_zapatos").value
					};
					
					var header = { "headers": {"Content-Type": "application/json"} };
					
						//$http.get('http://voluntariadocemex.com/api/registry?name='+name+'&last_name='+last_name+'&no_employee='+no_employee+'&document='+documento+'&cellphone='+cellphone+
						//'&email='+email+'&shirt_size='+shirt_size+'&shoe_size='+shoe_size )
					$http.post('http://voluntariadocemex.com/api/profile_edit', data , header, {timeout: 10000} )
					.success(function(result){
							
						if(result["api_status"] == 1){
							$scope.Ocultar_preloader();
							$scope.Mostar_Alert( "Los datos han sido actualizados." );							
						}
						
						else{
							$scope.Ocultar_preloader();
							$scope.Mostar_Alert( "Lo sentimos ha ocurrido un error, por favor valida tu acceso a internet." );
						}
		
					})
					.error(function(data, status, headers, config){
						$scope.Ocultar_preloader();
						$scope.Mostar_Alert('Lo sentimos, al parecer no tienes acceso a internet, intenta nuevamente');
					});
			}
				
		}
	};
	
	
	//CAMBIAR FOTO
	$scope.Change_photo = function(){
		base64 = "";
		var file = document.getElementById("imagen_perfil").files[0];
		var reader = new FileReader();
		reader.onloadend = function() {
				//console.log(reader.result);
				base64 = reader.result;
								
				//document.getElementById("imagen_perfil").files[0];
				document.getElementById("foto_new").setAttribute('src',base64);
				
				part = base64.split(",");
				base64 = part[1];	
				window.localStorage['foto_perfil_tmp'] = base64;
		}
		reader.readAsDataURL(file);		
	};
	
	
	$scope.TakePhotoProfile = function (options) {
	  
		var options = {
			quality : 30,
			targetWidth: 600,
			targetHeight: 600,
			sourceType: 1,
			destinationType:0
		};

		Camera.getPicture(options).then(function(imageData) {
			
				base64 = imageData;
				//document.getElementById("imagen_perfil").files[0];
				document.getElementById("foto_new").setAttribute('src',"data:image/jpeg;base64,"+base64);
				window.localStorage['foto_perfil_tmp'] = base64;
	 
			}, function(err) {
				//alert(err)
			 console.log(err);
		});
	};
	
	$scope.TakeLibraryProfile = function (options) {
	  
		var options = {
			quality : 40,
			targetWidth: 600,
			targetHeight: 600,
			sourceType: 0,
			mediaType:0,
			destinationType:0
		};

		Camera.getPicture(options).then(function(imageData) {
			
				base64 = imageData;
				//document.getElementById("imagen_perfil").files[0];
				document.getElementById("foto_new").setAttribute('src',"data:image/jpeg;base64,"+base64);
				window.localStorage['foto_perfil_tmp'] = base64;
	 
			}, function(err) {
				//alert(err)
			 console.log(err);
		});
	};
   
	
	
	
	//MOSTRAR PRELOADER
	$scope.Mostar_preloader = function() {
		$ionicLoading.show({
			template: 'Cargando...'
		}).then(function(){
		});
	};
	


});

